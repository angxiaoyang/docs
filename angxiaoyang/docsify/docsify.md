# docsify快速搭建个人博客

docsify是一个动态生成文档网站的工具。不同于GitBook、Hexo，它不会将.md转换为.html，所有转换工作都是在运行时进行。

## 1. 初始化文档

官方推荐`docs`作为文件名。

```ruby
PS E:\docsify200413\plus_project> docsify init docs

Initialization succeeded! Please run docsify serve docs
```

初始化会在`docs`目录下生成三个文件：

- `index.html` ：入口文件
- `README.md`：主页内容
- `.nojekyll`：防止Github忽略`下划线开头的文件`  

## 2. 启动本地预览 服务

```ruby
PS E:\docsify200413> docsify serve docs

Serving E:\docsify200413\docs now.
Listening at http://localhost:3000
```

## 3. 搭建博客

### a. 设置封面页

- 开启封面页：在`docs/index.html`中添加语句`coverpage: true`;  

  ```html
    <script>
      window.$docsify = {
        name: '',
        repo: '',
  	  coverpage: true
      }
    </script>
    <script src="//unpkg.com/docsify"></script>
  ```

- 封面页渲染：创建`_coverpage.md`文件。  

  ```markdown
  ![logo](_media/images/logo.jpg)
  
  # 昂小楊
  
  ## 菜鸟的docsify之旅
  
  *观自在菩萨/行深般若波罗蜜多时/照见五蕴皆空/渡一切苦厄*
  
  [GitHub](https://github.com/suiyidade/learngit)
  [Get Started](README.md)
  
  
  <!-- 背景图片 -->
  
  ![](_media/images/cov.jpg)
  ```

### b. 设置导航栏

类似封面页的设置，也可在HTML文件中设置，链接以`#/`开头。

- 开启导航页：在`docs/index.html`中添加语句`loadNavbar: true`;  

  ```html
    <script>
      window.$docsify = {
        name: '',
        repo: '',
  	  coverpage: true,
  	  loadNavbar: true
      }
    </script>
    <script src="//unpkg.com/docsify"></script>
  ```

- 导航页渲染：创建`_navbar.md`文件。


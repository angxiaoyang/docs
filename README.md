# docsify快速搭建个人博客

docsify是一个动态生成文档网站的工具。不同于GitBook、Hexo，它不会将.md转换为.html，所有转换工作都是在运行时进行。

## 1. 初始化文档

官方推荐`docs`作为文件名。

```ssh
PS E:\docsify200413\plus_project> docsify init docs

Initialization succeeded! Please run docsify serve docs
```

初始化会在`docs`目录下生成三个文件：

- `index.html` ：入口文件
- `README.md`：主页内容
- `.nojekyll`：防止Github忽略`下划线开头的文件`  

## 2. 启动本地预览 服务

```ssh
PS E:\docsify200413> docsify serve docs

Serving E:\docsify200413\docs now.
Listening at http://localhost:3000
```

*可在进入`http://localhost:3000`，本地实时查看渲染效果*

## 3. 搭建博客

### a. 设置封面页

- 开启封面页：在`docs/index.html`中添加语句`coverpage: true`;  

  ```html
    <script>
      window.$docsify = {
        name: '',
        repo: '',
  	  coverpage: true
      }
    </script>
    <script src="//unpkg.com/docsify"></script>
  ```

- 封面页渲染：创建`_coverpage.md`文件。  

  ```markdown
  ![logo](_media/images/logo.jpg)
  
  # 昂小楊
  
  ## 菜鸟的docsify之旅
  
  *观自在菩萨/行深般若波罗蜜多时/照见五蕴皆空/渡一切苦厄*
  
  [GitHub](https://github.com/suiyidade/learngit)
  [Get Started](README.md)
  ```

### b. 设置侧边标题与挂件

在`docs/index.html`中设置，其中`name`是侧边栏标题  
`repo`会在页面右上角渲染GitHub corner插件，点击可跳转指定地址

```html
<body>
  <div id="app"></div>
  <script>
    window.$docsify = {
      name: '诗酒趁年华',
      repo: 'https://gitee.com/angxiaoyang/docs',
	  coverpage: true,
    }
  </script>
  <script src="//unpkg.com/docsify"></script>
  <script src="//unpkg.com/docsify/lib/docsify.min.js"></script>
</body>
```

### c. 设置网页标题

在`docs/index.html`中添加`<title>诗酒趁年华</title>`

## 4. 使用GiteePages部署博客网站

### a. 将整个工程用`git`push到`码云Gitee`

在`Gitee`上传本机的`SSH公钥`，在用户主目录下的`.ssh/id_rsa.pub`

在gitee上新建一个项目库`docs`

把本地库与Gitee的远程库关联

` git remote add origin git@gitee.com:angxiaoyang/docs.git`

推送本地库`git push -u origin master`

### b. 配置GiteePages服务

![GiteePages](./media/images/GiteePages.png)

## 5. 相关链接



### [昂小楊](https://gitee.com/angxiaoyang/docs)

### [docsify](https://docsify.js.org/#/zh-cn/)

### [Typora](https://www.typora.io/)

